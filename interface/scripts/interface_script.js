url = 'ws://localhost:9090'

function init_rosbridge() {
  var ros = new ROSLIB.Ros();

  var connecting = document.getElementById('connecting');
  var connected = document.getElementById('connected');
  var closed = document.getElementById('closed');
  var error = document.getElementById('error');

  var errorOcurred = false;

  // If there is an error on the backend, an 'error' emit will be emitted.
  ros.on('error', function(error_) {
    connecting.style.display = connected.style.display = closed.style.display = 'none';
    error.style.display = 'inline';
    console.log(error_);
    errorOcurred = true;
  });

  // Find out exactly when we made a connection.
  ros.on('connection', function() {
    console.log('Connection made!');
    connecting.style.display = closed.style.display = error.style.display = 'none';
    connected.style.display = 'inline';
  });

  ros.on('close', function() {
    if(errorOcurred) return;
    console.log('Connection closed.');
    connecting.style.display = connected.style.display = error.style.display = 'none';
    closed.style.display = 'inline';
  });

  // Create a connection to the rosbridge WebSocket server.
  ros.connect('ws://localhost:9090');
}

function init_remoteLaunch() {
  // Connect to ROS. Global variable
  ros = new ROSLIB.Ros({
    url : url
  });

  ros.on('error', function(error) {
    alert('Error connecting to rosbridge');
    console.log(error);
    return ;
  });

  var RLClient = new REMOTELAUNCH.Client({
    ros : ros,
    serverName : '/remote_launch_server',
    topic : '/list_launch_files',
  });

  tooltipsList = tooltipsList || {};

  var RLInteract = new REMOTELAUNCH.ListInteract({
    client : RLClient,
    canvas : 'fileCanvas',
    inputDiv : 'fileInputs',
    numberOfColumns : 2,
    scale : 1,
    ignoreThese : ignoreThese,
    tooltipsDiv : 'tooltips',
    tooltipsList : tooltipsList,
  });

  RLClient.eventEmitter.on('fileStart', handleFileStart);
  RLClient.eventEmitter.on('fileStop', handleFileStop);
}

function handleFileStart(fileName) {
  if(fileName == 'Editor') {
    interface3D();
  }

  else if(fileName == 'View') {
    interface2D();  
  }
}

function handleFileStop(fileName) {
  if(fileName == 'Editor') {
    deleteInterface3D();
  }

  else if(fileName == 'View') {
    deleteInterface2D();
  }
}

function init_editTrajectories() {

  // Define list of ids to ignore
  ignoreThese = [0];

  // Define list of tooltips
  tooltipsList = {
      'Editor' : 'Launch trajectories editor. Use a mouse to work with this module',
      'Save Traj.' : 'Save current trajectory to jarvis_web_scripts/config. Specify filename suffix with file:=suffix',
      'Load Traj.' : 'Load trajectory from jarvis_web_scripts/config. Must reload editor afterwards. Specify filename suffix with file:=suffix',
      'Reload Editor' : 'Necessary after loading a new trajectory',
      'Start Jarvis' : 'Start the robot, which will follow the loaded trajectory. WARNING: First movement is not safe if far from trajectory!',
      'Stop Jarvis' : 'Stop the robot'
  };

  // Define using for 2D for trajectory 3D viewer
  using_for_2D = true;

  init_remoteLaunch();
}

function interface3D() {
  // Create the main viewer.
  var viewer3D = new ROS3D.Viewer({
    divID : 'map',
    width : 800,
    height : 700,
    antialias : true,
    near: 0.001,
    far: 1000,
    cameraZoomSpeed: 1,
    cameraPose: {
      x : 0,
      y : -10,
      z : 50
    }
  });

  // Setup a client to listen to TFs.
  var tfClient = new ROSLIB.TFClient({
    ros : ros,
    angularThres : 0.01,
    transThres : 0.01,
    rate : 10.0,
    fixedFrame : '/map'
  });

  // Setup the marker client.
  
  var imClient = new ROS3D.InteractiveMarkerClient({
    ros : ros,
    tfClient : tfClient,
    topic : '/trajectory_editor',
    camera : viewer3D.camera,
    rootObject : viewer3D.selectableObjects
  });
  
  var gridClient = new ROS3D.OccupancyGridClient({
    ros : ros,
    rootObject : viewer3D.scene
  });
  
  var arrow = new ROS3D.Arrow({
    length        : 0.7,
    shaftDiameter : 0.1
  });
  arrow.setColor(0x000000);

  var arrowNode = new ROS3D.SceneNode({
    tfClient : tfClient,
    frameID  : 'base_footprint',
    object   : arrow, 
  });

 viewer3D.scene.add(arrowNode);
}

function deleteInterface3D(){
	$('#map').empty();
}

function interface2D() {
	// Create the main viewer.
	var viewer2D = new ROS2D.Viewer({
		divID : 'map',
		width : 800,
		height : 700
	});

	// Setup the map client.
	var gridClient = new ROS2D.OccupancyGridClient({
		ros : ros,
		rootObject : viewer2D.scene,
		//Use this property in case of continuous updates			
		continuous: true
	});
	// Scale the canvas to fit to the map
	gridClient.on('change', function() {
		viewer2D.scaleToDimensions(gridClient.currentGrid.width, gridClient.currentGrid.height);
		viewer2D.shift(gridClient.currentGrid.pose.position.x, gridClient.currentGrid.pose.position.y);
	});

  var nav = NAV2D.OccupancyGridClientNav({
    ros : ros,
    rootObject : viewer2D.scene,
    viewer : viewer2D,
    serverName : '/pose_goal',
		robot_pose : '/pose_ground_truth',
		use_mouse : false,
  });
}

function deleteInterface2D(){
	$('#map').empty();
}
